package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class CreateAccountPage {

    private WebDriver driver;
    private By genderRadioButtonMaleID1 = By.id("id_gender1");
    private By genderRadioButtonFemaleID2 = By.id("uniform-id_gender2");
    private By firstNameInputId = By.id("customer_firstname");
    private By lastNameInputId = By.id("customer_lastname");
    private By passwordInputID = By.id("passwd");
    private Select dayBirth, monthBirth, yearBirth, state;
    private By fnameAdressID = By.id("firstname");
    private By lnameAdressID = By.id("lastname");
    private By adressInputID= By.id("address1");
    private By cityInputID = By.id("city");
    private By zipInputID = By.id("postcode");
    private By mPhoneID = By.id("phone_mobile");
    private By adressAliasID = By.id("alias");
    private By buttonId = By.id("submitAccount");
    private By signOutButton = By.cssSelector("#header > div.nav > div > div > nav > div:nth-child(2) > a");

    public CreateAccountPage (WebDriver driver) {
        this.driver=driver;
    }

    public void populateData (List<String> data) {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(adressAliasID));

     /* 0- gender
        1- first name
        2- second name
        3- password
        4- day birth
        5- month birth
        6- year birth
        7- first name address
        8- second name address
        9- address
        10- city
        11- state
        12- zip
        13- phone
        14- alias address */

        if(data.get(0).equals("male")){
           WebElement male = driver.findElement(genderRadioButtonMaleID1);
           male.click();
        }else if (data.get(0).equals("female")){
           WebElement female = driver.findElement(genderRadioButtonFemaleID2);
           female.click();
        }
        WebElement firstName = driver.findElement(firstNameInputId); firstName.sendKeys(data.get(1));
        WebElement lastName = driver.findElement(lastNameInputId); lastName.sendKeys(data.get(2));
        WebElement password= driver.findElement(passwordInputID); password.sendKeys(data.get(3));
        dayBirth = new Select(driver.findElement(By.id("days")));
        dayBirth.selectByValue(data.get(4));
        monthBirth = new Select(driver.findElement(By.id("months")));
        switch(data.get(5)) {
            case "January":
                monthBirth.selectByValue(String.valueOf(1));break;
            case "February" :
                monthBirth.selectByValue(String.valueOf(2));break;
            case "March" :
                monthBirth.selectByValue(String.valueOf(3));break;
            case "April" :
                monthBirth.selectByValue(String.valueOf(4));break;
            case "May" :
                monthBirth.selectByValue(String.valueOf(5));break;
            case "June" :
                monthBirth.selectByValue(String.valueOf(6));break;
            case "July" :
                monthBirth.selectByValue(String.valueOf(7));break;
            case "August" :
                monthBirth.selectByValue(String.valueOf(8));break;
            case "September" :
                monthBirth.selectByValue(String.valueOf(9));break;
            case "October" :
                monthBirth.selectByValue(String.valueOf(10));break;
            case "November" :
                monthBirth.selectByValue(String.valueOf(11));break;
            case "December" :
                monthBirth.selectByValue(String.valueOf(12));break;
        }
        yearBirth = new Select((driver.findElement(By.id("years"))));
        yearBirth.selectByValue(data.get(6));
        WebElement fNameAdress = driver.findElement(fnameAdressID); fNameAdress.clear(); fNameAdress.sendKeys(data.get(7));
        WebElement lNameAdress = driver.findElement(lnameAdressID); lNameAdress.clear(); lNameAdress.sendKeys(data.get(8));
        driver.findElement(adressInputID).sendKeys(data.get(9));
        driver.findElement(cityInputID).sendKeys(data.get(10));
        state = new Select(driver.findElement(By.id("id_state")));
        state.selectByVisibleText(data.get(11));
        driver.findElement(zipInputID).sendKeys(data.get(12));
        driver.findElement(mPhoneID).sendKeys(data.get(13));
        driver.findElement(adressAliasID).sendKeys(data.get(14));
        driver.findElement(buttonId).click();
    }
    public String getLogInStatus() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(signOutButton));
      return   driver.findElement(signOutButton).getText();
    }
}
