package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckOutPage {

    private WebDriver driver;
    private final By proceedSummaryID = By.cssSelector("#center_column > p.cart_navigation.clearfix > a.button.btn.btn-default.standard-checkout.button-medium");
    private final By emailFieldID = By.id("email");
    private final By passwordFieldID = By.id("passwd");
    private final By signInButtonID = By.id("SubmitLogin");
    private final By proceedAddressID = By.cssSelector("#center_column > form > p > button");
    private final By checkBoxID = By.id("cgv");
    private final By proceedShippingID = By.cssSelector("#form > p > button");
    private final By bankWireID = By.cssSelector("#HOOK_PAYMENT > div:nth-child(1) > div > p > a");
    private final By confirmOrderButtonID = By.cssSelector("#cart_navigation > button");
    private final By totalPriceID = By.id("total_price");
    private final By stateDeliveryID = By.cssSelector("#address_delivery > li.address_country_name");
    private final By phoneBillingID = By.cssSelector("#address_invoice > li.address_phone_mobile");
    private final By shippingPriceID = By.cssSelector("#form > div > div.delivery_options_address > div.delivery_options > div > div > table > tbody > tr > td.delivery_option_price > div");
    private final By continueShoppingID = By.cssSelector("#center_column > div > p > a");
    private final By titleID = By.cssSelector("#center_column > div > p");

    public CheckOutPage (WebDriver driver){
        this.driver=driver;
    }
    public void proceedSummary() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(proceedSummaryID));
        driver.findElement(proceedSummaryID).click();
    }
    public String summaryVerificationPrice() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(totalPriceID));
        return driver.findElement(totalPriceID).getText();
    }
    public void signIn(String emailInput, String passInput){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(signInButtonID));
        WebElement signInButton = driver.findElement(signInButtonID);
        if(signInButton.isDisplayed()){
            driver.findElement(emailFieldID).sendKeys(emailInput);
            driver.findElement(passwordFieldID).sendKeys(passInput);
            signInButton.click();
        }
    }
    public void proceedAddress() {
        driver.findElement(proceedAddressID).click();
    }
    public String addressVerificationStateDelivery(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(stateDeliveryID));
        return driver.findElement(stateDeliveryID).getText();
    }
    public String addressVerificationPhoneBilling(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(phoneBillingID));
        return driver.findElement(phoneBillingID).getText();
    }
    public void proceedShipping () {
        driver.findElement(checkBoxID).click();
        driver.findElement(proceedShippingID).click();
    }
    public String shippingPriceVerification(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(shippingPriceID));
        return driver.findElement(shippingPriceID).getText();
    }
    public void payBankWire(){
        driver.findElement(bankWireID).click();
    }
    public Boolean checkAvailabilityContinueShopping(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(continueShoppingID));
        return driver.findElement(continueShoppingID).isDisplayed();
    }
    public void confirmOrder() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(confirmOrderButtonID));
        driver.findElement(confirmOrderButtonID).click();
    }
    public String verifyTheTitle() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(titleID));
        return driver.findElement(titleID).getText();
    }
}
