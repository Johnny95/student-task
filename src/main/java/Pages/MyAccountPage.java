package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyAccountPage {
    private final By infoAccountID = By.cssSelector("#center_column > p");
    private WebDriver driver;

    public MyAccountPage (WebDriver driver){
        this.driver=driver;
    }

    public String getinfoAccount () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(infoAccountID));
        return driver.findElement(infoAccountID).getText();
    }
}
