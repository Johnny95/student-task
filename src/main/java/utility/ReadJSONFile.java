package utility;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadJSONFile {

    public static void addToArray(String email, String password) throws IOException {

        FileWriter file = null;

        JSONObject jsonObject;
        JSONParser parser = new JSONParser();

        try {
            jsonObject = (JSONObject) parser.parse(new FileReader("src/JSONSignIn.json"));
            JSONArray jsonArray = (JSONArray) jsonObject.get("users");
            JSONObject newUser = new JSONObject();
            newUser.put("email", email);
            newUser.put("password", password);
            jsonArray.add(newUser);

            file = new FileWriter("src/JSONSignIn.json");
            file.append(jsonObject.toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            file.flush();
            file.close();
        }
    }
}
